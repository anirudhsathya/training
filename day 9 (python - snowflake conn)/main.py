''' Main file containing function calls and class object creation for connecting python with snowflake
also has dataframe creation and its loading CSV -> DF -> SnnowFlake'''

from pkg import py2sql
import pandas as pd

py2sql_obj = py2sql.python2sf()

py2sql_obj.snow_connector()

df = pd.read_csv(
    r'C:\Users\anirudh.s\OneDrive - Happiest Minds Technologies Limited\Desktop\Practice\day 9 (python - snowflake conn)\matches.csv')

py2sql_obj.pprint(df)
df = df.iloc[:, :7]

df.columns = list(map(lambda x: x.upper(), df.columns))
# print(df.info())
''' SF query to create table
create or replace table firstdb.public.matches(
id int,
season int,
city varchar(30),
date varchar(30),
team1 varchar(30),
team2 varchar(30),
toss_winner varchar(30)
);
'''


py2sql_obj.write_df(df)
