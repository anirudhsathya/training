'''Custom logger file'''

import logging
FILE = r"C:\Users\anirudh.s\OneDrive - Happiest Minds Technologies Limited\Desktop\Practice\day 9 (python - snowflake conn)\pkg\py2sql.log"


class logger():
    def __init__(self):
        self.logger = logging.getLogger(__name__)

        self.logger.setLevel(logging.INFO)
        self.formatter = logging.Formatter(
            '%(asctime)s : %(levelname)s : %(name)s : %(funcName)s : %(message)s')

        self.file_handler = logging.FileHandler(FILE, mode='w')
        self.file_handler.setFormatter(self.formatter)

        self.logger.addHandler(self.file_handler)
