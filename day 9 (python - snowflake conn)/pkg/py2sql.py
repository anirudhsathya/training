''' Snowflake connection using snowflake.connector and write_pandas to write the data to SnowFlake
Have used custom logger to log all the outputs I get'''


import snowflake.connector as sc
from snowflake.connector.pandas_tools import write_pandas
import logging
import pkg.py2sql_logger as pylog
from retry import retry
import os
from dotenv import load_dotenv

load_dotenv()
# log.basicConfig(filename=FILE, level=log.INFO,
# format='%(asctime)s : %(levelname)s : %(name)s : %(funcName)s : %(message)s', filemode='w')  # log files format


class python2sf():
    def pprint(self, msg):
        self.log.info(f"\n{msg}")

    ''' To establish connection with snowflake
    the username, password, account ID, warehouse name, database name and schema 
    which is to be accessed
    Retru decorator to make it try several times
    '''
    @retry(delay=3, tries=3)
    def snow_connector(self):
        try:
            self.log.info('Connection trial started')

            # Establishing connection with Snowflake uisng env variables
            self.conn = sc.connect(user=os.getenv("SNOWFLAKE_USERNAME"),
                                   password=os.getenv("SNOWFLAKE_PASSWORD"),
                                   account=os.getenv("SNOWFLAKE_ACCOUNT"),
                                   warehouse=os.getenv("SNOWFLAKE_WAREHOUSE"),
                                   database='FIRSTDB', schema='public')
        except:
            self.log.info(
                'Connection Failed. Retrying in 3...2...1...')
            raise
        else:
            self.log.info("Connection successful")

    ''' Writing data into snowflake database with write_pandas.
     But for thsi we need to create a table in the snowflake before we write the data to it
     Variables success, nchunks and nrows represent the FUNCTION execution status, number of chunks 
     and the number of rows the write pandas returns after execution'''

    def write_df(self, df):
        self.log.info("Started writing...")
        self.success, self.nchunks, self.nrows, _ = write_pandas(
            self.conn, df, 'MATCHES')                   # Writing pandas dataframe to snowflake
        self.log.info(
            f'Upload to SF complete. {self.success}, {self.nchunks}, {self.nrows}')

    def __init__(self):
        self.conn = None
        self.cursor = None
        self.log_obj = pylog.logger()           # custom logger
        self.log = self.log_obj.logger
        self.log.info("Logger created")
